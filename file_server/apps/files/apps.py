from django.apps import AppConfig


class FilesConfig(AppConfig):
    name = 'file_server.apps.files'
